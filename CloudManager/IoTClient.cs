﻿using System;
using System.Text;
using System.Threading.Tasks;
using System.Diagnostics;
using Microsoft.Azure.Devices.Client;
using Windows.Foundation.Collections;

namespace CloudManager
    
{

    class IoTClient
    {
        // Sent To Test Device with Connection String (Http or Mqtt)
        private const string DeviceConnectionString = "HostName=NydorIoTHubDb.azure-devices.net;DeviceId=TestDevice;SharedAccessKey=bjxbfgMIVjIFnzXDda9FsHOtvkUGmWHY2yKpqOqgQso=";
        //static DeviceClient deviceClient = DeviceClient.CreateFromConnectionString(DeviceConnectionString, TransportType.Http1);
        static DeviceClient deviceClient = DeviceClient.CreateFromConnectionString(DeviceConnectionString, TransportType.Mqtt);


        // Send To Test Device with iotHubUri (Mqtt)
        //static DeviceClient deviceClient = DeviceClient.Create(iotHubUri, new DeviceAuthenticationWithRegistrySymmetricKey("TestDevice", deviceKey), TransportType.Mqtt);
        //static string iotHubUri = "NydorIoTHubDb.azure-devices.net";
        //static string deviceKey = "bjxbfgMIVjIFnzXDda9FsHOtvkUGmWHY2yKpqOqgQso=";
        public async static Task Start()
        {
            try
            {
                Debug.WriteLine("Starting ..........");

                // Method Call processing will be enabled when the first method handler is added.
                // setup a calback for the 'GetDeviceName' method
                await deviceClient.SetMethodHandlerAsync("GetDeviceName", GetDeviceName, new DeviceData("DeviceClientMethodMqttSample"));

                ReceiveC2dAsync();


            }
            catch (Exception ex)
            {
                Debug.WriteLine("Start: Exception thrown: " + ex.Message);
            }

        }

        public async static Task StartSendingMessage(object in_Message)
        {
            try
            {


                string inputMessage = in_Message.ToString();

                //Debug.WriteLine("Data Received: " + inputMessage);

                await SendEvent(inputMessage);

              
            }
            catch (Exception ex)
            {
                Debug.WriteLine("StartSendingMessage: Exception thrown: " + ex.Message);

            }

        } //end of method


        static async Task SendEvent(string message)
        {

            //string dataBuffer;
            //Debug.WriteLine("Device sending {0} messages to IoTHub...\n", MESSAGE_COUNT);
            //for (int count = 0; count < MESSAGE_COUNT; count++)
            // {

            //dataBuffer = string.Format("Msg from UWP: {0}_{1}", count, Guid.NewGuid().ToString());
            //dataBuffer = string.Format("Msg from UWP: {0}_{1}", count, message);

            //Message eventMessage = new Message(Encoding.UTF8.GetBytes(dataBuffer));
            Message eventMessage = new Message(Encoding.UTF8.GetBytes(message));
            // Debug.WriteLine("\t{0}> Sending message: {1}, Data: [{2}]", DateTime.Now.ToLocalTime(), count, dataBuffer);
            await deviceClient.SendEventAsync(eventMessage);

            // }

            /*
             Message eventMessage = new Message(Encoding.UTF8.GetBytes(message));
             await deviceClient.SendEventAsync(eventMessage);
             */

        }

        

        private static async void ReceiveC2dAsync()
        {
            try
            {
                Debug.WriteLine("------------------------------------------------");
                Debug.WriteLine("\nReceiving cloud to device messages from service");
                //deviceClient = DeviceClient.Create(iotHubUri, new DeviceAuthenticationWithRegistrySymmetricKey("TestDevice", deviceKey), TransportType.Mqtt);

                while (true)
                {
                    Message receivedMessage = await deviceClient.ReceiveAsync();
                    if (receivedMessage == null) continue;

                    string message = Encoding.ASCII.GetString(receivedMessage.GetBytes());
                    Debug.WriteLine("----- MESSAGE RECEIVED :" + message);

                    // Send Command received to Nst Application
                    //await sendToNstAppwithLaunchUri(message);


                    // DO NOT DELETE
                    // DOWNLOAD DB FILE


                    //bool result = message.Equals("Database update", StringComparison.Ordinal);
                    //if (result == true)
                    //{
                    //Debug.WriteLine("Start downloading process .........................");

                    //CloudStorageAccount storageAccount = new CloudStorageAccount(
                    //       new Microsoft.WindowsAzure.Storage.Auth.StorageCredentials(
                    //       "nydorstoragetest",
                    //       "0ppe15mnVCdFPfS81Zxj88jRIgAz6hCRorsaKgVEA7jyqd7NMfGr4sjhW0iWkIfpBSb38BWHZB8s4LFS/RRD+Q=="), true);

                    //// Create a blob client.
                    //CloudBlobClient blobClient = storageAccount.CreateCloudBlobClient();

                    //// Get a reference to the container named “gatewaydbcontainer”
                    //CloudBlobContainer container = blobClient.GetContainerReference("gatewaydbcontainer");

                    //// Get a reference to a blob named "gateway.db".
                    //CloudBlockBlob blockBlob = container.GetBlockBlobReference("gateway.db");

                    ////  create a local file
                    //StorageFile file = await ApplicationData.Current.LocalFolder.CreateFileAsync("Gateway.db", CreationCollisionOption.ReplaceExisting);

                    ////  download from Azure Storage
                    //await blockBlob.DownloadToFileAsync(file);

                    //TestSqliteDatabase();

                    //}

                    //notifies IoT Hub that the message has been successfully processed
                    await deviceClient.CompleteAsync(receivedMessage);
                }
            }
            catch (Exception ex)
            {
                Debug.WriteLine("ReceiveC2dAsync: Exception thrown: " + ex.Message);

            }
        }

        private async static Task sendToNstAppwithLaunchUri(string message)
        {
            try
            {
                var options = new Windows.System.LauncherOptions();
                //UWP application package family
                //options.TargetApplicationPackageFamilyName = "UWP-IoT-a1429fd3-3896-4330-8ba8-2c762acb3cb9_699efvd6fdawc";


                //Cloud Manager package family
                //options.TargetApplicationPackageFamilyName = "4d041419-6c8c-49ee-9d9c-126edc60ad5e_dryy15wfsn6zj";

                // Nst App package Family name
                options.TargetApplicationPackageFamilyName = "fe1dfef0-4e97-48f6-85c1-4e13380c3b51_dryy15wfsn6zj";

        
                ValueSet sentData = new ValueSet();
                // Sets token as command received
                // Sets sender as CloudManager
                sentData.Add("Token", message);
                sentData.Add("Sender", "CloudManager");

                //Uri uri = new Uri("nydor.azure.interface:?data=12345");
                Uri uri = new Uri("nst.gateway.nstapplication:");
                bool success = await Windows.System.Launcher.LaunchUriAsync(uri, options, sentData);
                if (!success)
                {
                    Debug.WriteLine("Nst Application can not be launched");
                    //textBlock.Text += "Cloud Client can not be launched";
                    //await ((App)Application.Current).WriteMessage("Cloud Client can not be launched");

                }
                sentData.Clear();
            }
            catch (Exception ex)
            {
                Debug.WriteLine("sendToNstAppwithLaunchUri: Exception thrown: " + ex.Message);

            }

        }





        //private static void TestSqliteDatabase()
        //{

        //    SqliteEngine.UseWinSqlite3(); //Configuring library to use SDK version of SQLite
        //    using (SqliteConnection db = new SqliteConnection("Filename=Gateway.db"))
        //    {
        //        try
        //        {
        //            db.Open();
        //            Grab_Entries();
        //        }
        //        catch (SqliteException e)
        //        {
        //            //Do nothing
        //        }
        //    }
        //}

        //private static void Grab_Entries()
        //{
        //    List<String> entries = new List<string>();

        //    using (SqliteConnection db = new SqliteConnection("Filename=Gateway.db"))
        //    {
        //        SqliteCommand selectCommand;
        //        SqliteDataReader query;
        //        db.Open();

        //        // ------------ Read Gateway table ------------------
        //        selectCommand = new SqliteCommand("SELECT GW_Unique_ID from Gateways", db);
        //        try
        //        {
        //            query = selectCommand.ExecuteReader();
        //            while (query.Read())
        //            {
        //                Debug.WriteLine("Gateway:" + query.GetString(0));
        //            }
        //        }
        //        catch (SqliteException error)
        //        {
        //            //Handle error
        //            Debug.WriteLine("SqliteException: " + error.Message);
        //        }


        //        //--------------Read NstApplication table -----------------
        //        selectCommand = new SqliteCommand("SELECT application_name from NstApplications", db);

        //        try
        //        {
        //            query = selectCommand.ExecuteReader();
        //            while (query.Read())
        //            {
        //                Debug.WriteLine("Application Name: " + query.GetString(0));
        //            }
        //        }
        //        catch (SqliteException error)
        //        {
        //            //Handle error
        //            Debug.WriteLine("SqliteException: " + error.Message);
        //        }


        //        // -------------- Read RunningApp table --------------------
        //        selectCommand = new SqliteCommand("SELECT NstApplicationID, GatewayID from RunningApps", db);

        //        try
        //        {
        //            query = selectCommand.ExecuteReader();
        //            while (query.Read())
        //            {
        //                Debug.WriteLine("   Application Id: " + query.GetString(0) + " Gateway Id: " + query.GetString(1));
        //            }
        //        }
        //        catch (SqliteException error)
        //        {
        //            //Handle error
        //            Debug.WriteLine("SqliteException: " + error.Message);
        //        }
        //        Debug.WriteLine("Running Apps: ");


        //        // ----------  Read CMG table -----------------
        //        selectCommand = new SqliteCommand("SELECT CMG_Unique_ID, GatewayID from CMGs", db);

        //        try
        //        {
        //            query = selectCommand.ExecuteReader();
        //            while (query.Read())
        //            {
        //                Debug.WriteLine("   CMG Id: " + query.GetString(0) + " GatewayID: " + query.GetString(1));
        //            }
        //        }
        //        catch (SqliteException error)
        //        {
        //            //Handle error
        //            Debug.WriteLine("SqliteException: " + error.Message);
        //        }
        //        Debug.WriteLine("CMGs: ");



        //        // --------------- Read CM_Type ---------------------
        //        selectCommand = new SqliteCommand("SELECT CM_TypeID, CMGID from CM_Type", db);

        //        try
        //        {
        //            query = selectCommand.ExecuteReader();
        //            while (query.Read())
        //            {
        //                Debug.WriteLine("   CM TypeID: " + query.GetString(0) + " CMGID: " + query.GetString(1));
        //            }
        //        }
        //        catch (SqliteException error)
        //        {
        //            //Handle error
        //            Debug.WriteLine("SqliteException: " + error.Message);
        //        }
        //        Debug.WriteLine("CM Type: ");


        //        // Read ----------- CMDs ----------------
        //        selectCommand = new SqliteCommand("SELECT CMD_Unique_ID, CMGID from CMDs", db);

        //        try
        //        {
        //            query = selectCommand.ExecuteReader();
        //            while (query.Read())
        //            {
        //                Debug.WriteLine("   CMD_Unique_ID: " + query.GetString(0) + " CMGID: " + query.GetString(1));
        //            }
        //        }
        //        catch (SqliteException error)
        //        {
        //            //Handle error
        //            Debug.WriteLine("SqliteException: " + error.Message);
        //        }
        //        Debug.WriteLine("CMDs: ");


        //        // ---------- Read Devices --------------
        //        selectCommand = new SqliteCommand("SELECT DVC_Unique_ID, CMDID from Devices", db);

        //        try
        //        {
        //            query = selectCommand.ExecuteReader();
        //            while (query.Read())
        //            {
        //                Debug.WriteLine("   DVC_Unique_ID: " + query.GetString(0) + " CMGID: " + query.GetString(1));
        //            }
        //        }
        //        catch (SqliteException error)
        //        {
        //            //Handle error
        //            Debug.WriteLine("SqliteException: " + error.Message);
        //        }
        //        Debug.WriteLine("Devices: ");


        //        db.Close();
        //    }

        //}


        // Use it for testing DEVICE METHOD
        static Task<MethodResponse> GetDeviceName(MethodRequest methodRequest, object userContext)
        {
            MethodResponse retValue;
            if (userContext == null)
            {
                retValue = new MethodResponse(new byte[0], 500);
            }
            else
            {
                var d = userContext as DeviceData;
                //string result = "{\"name\":\"" + d.Name + "\"}";
                string result = "{\"value\":\"" + "SKATOULES!" + "\"}";
                //string result = "SKATOULES";
                retValue = new MethodResponse(Encoding.UTF8.GetBytes(result), 200);
                
            }
            Task<MethodResponse> xx = Task.FromResult(retValue);
            return xx;
        }

        // Use it for testing DEVICE METHOD
        class DeviceData
        {
            public DeviceData(string myName)
            {
                this.Name = myName;
            }

            public string Name
            {
                get; set;
            }
        }

    }
}
